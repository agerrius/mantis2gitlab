﻿using System;
using System.IO;

namespace Mantis2Gitlab
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length < 7)
            {
                Console.WriteLine("Usage:");
                Console.WriteLine(
                    "Mantis2Gitlab <mantisUrl> <mantisToken> <gitlabUrl> <projectId> <gitlabToken> <fromID> <toID> <flags>");
                Console.WriteLine("");
                Console.WriteLine("Possible flags:");
                Console.WriteLine("  -D <path> download Mants issue attachments to the given path");
                Console.WriteLine("  -P preserves the Mantis ID in Gitlab (existing issues are overwritten)");
                Console.WriteLine("  -V verbose mode, print issues to screen");
                return;
            }

            string mantisUrl = args[0];
            string mantisToken = args[1];
            string gitlabUrl = args[2];
            int gitlabProject = Convert.ToInt32(args[3]);
            string gitlabToken = args[4];
            int fromId = Convert.ToInt32(args[5]);
            int toId = Convert.ToInt32(args[6]);

            bool preserveId = false;
            bool downloadAttachments = false;
            string downloadPath = string.Empty;
            bool verbose = false;

            for (int i = 7; i < args.Length; i++)
            {
                if (args[i] == "-D")
                {
                    downloadAttachments = true;
                    downloadPath = args[i + 1];
                    Console.WriteLine($"Downloading attachments to {downloadPath}");
                }
                else if (args[i] == "-P")
                {
                    preserveId = true;
                    Console.WriteLine("Preserving Mantis issue ID");
                }
                else if (args[i] == "-V")
                {
                    verbose = true;
                }
            }

            Mantis mantis = new Mantis(mantisUrl, mantisToken);
            Gitlab gitlab = new Gitlab(gitlabUrl, gitlabProject, gitlabToken, preserveId);

            for (int i = fromId; i <= toId; i++)
            {
                Console.WriteLine($"Processing issue {i}");
                Issue issue = mantis.GetIssue(i);

                if (issue == null)
                {
                    Console.WriteLine("Issue not found, skipping...");
                }
                else
                {
                    if (verbose)
                    {
                        issue.Print();
                    }

                    int newIssueId = gitlab.CreateIssue(issue);
                    if (downloadAttachments)
                    {
                        foreach (Attachment attachment in issue.Attachments)
                        {
                            string attachmentPath = Path.Combine(downloadPath,
                                Path.Combine(newIssueId.ToString(), attachment.Name));
                            issue.SaveAttachment(attachment, attachmentPath);
                            gitlab.AddNote(newIssueId, $"Attachment stored in {attachmentPath}", attachment.CreatedAt);
                        }
                    }
                }
            }

            gitlab.SetRelations();
        }
    }
}