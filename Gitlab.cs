﻿using System;
using System.Collections.Generic;
using System.Threading;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace Mantis2Gitlab
{
    public class Gitlab
    {
        struct RelationData
        {
            public int FromId;
            public int ToId;
            public bool IsDuplicate;
            public DateTime UpdatedAt;

            public RelationData(int from, int to, bool dupl, DateTime updated)
            {
                FromId = from;
                ToId = to;
                IsDuplicate = dupl;
                UpdatedAt = updated;
            }
        }

        private string _hostUrl;
        private int _projectId;
        private string _accessToken;
        private RestClient client;
        private bool _preserveIssueId;
        private Dictionary<string, int> _milestoneDict;
        private List<RelationData> _relations;

        public Gitlab(string hostUrl, int projectId, string accessToken, bool preserveId = false)
        {
            _hostUrl = hostUrl;
            _projectId = projectId;
            _accessToken = accessToken;
            _preserveIssueId = preserveId;
            _relations = new List<RelationData>();

            client = new RestClient($"{_hostUrl}/api/v4/projects/{_projectId}");

            InitMilestones();
        }

        bool ClientDelete(RestRequest request)
        {
            var response = client.Delete(request);
            return response.IsSuccessful;
        }

        bool ClientGet(RestRequest request, out string content)
        {
            var response = client.Get(request);
            content = response.Content;
            if (response.StatusDescription == "Too Many Requests")
            {
                Console.WriteLine("Too many requests, taking a pause...");
                Thread.Sleep(30000);
                return ClientGet(request, out content);
            }
            else if (!response.IsSuccessful)
            {
                Console.WriteLine(response.StatusDescription);
            }

            return response.IsSuccessful;
        }

        bool ClientPost(RestRequest request, out string content)
        {
            var response = client.Post(request);
            content = response.Content;
            if (response.StatusDescription == "Too Many Requests")
            {
                Console.WriteLine("Too many requests, taking a pause...");
                Thread.Sleep(30000);
                return ClientPost(request, out content);
            }
            else if (!response.IsSuccessful)
            {
                Console.WriteLine(response.StatusDescription);
            }

            return response.IsSuccessful;
        }

        public void SetRelations()
        {
            foreach (RelationData relation in _relations)
            {
                Console.WriteLine($"Adding relation from {relation.FromId} to {relation.ToId}");
                AddRelation(relation.FromId, relation.ToId, relation.IsDuplicate, relation.UpdatedAt);
            }
        }

        void DeleteExistingIssue(int id)
        {
            RestRequest request = new RestRequest($"issues/{id}");
            request.Method = Method.GET;
            request.AddHeader("PRIVATE-TOKEN", _accessToken);

            string content;
            if (ClientGet(request, out content))
            {
                RestRequest deleteRequest = new RestRequest($"issues/{id}");
                deleteRequest.Method = Method.DELETE;
                deleteRequest.AddHeader("PRIVATE-TOKEN", _accessToken);
                ClientDelete(deleteRequest);
            }
        }

        public int CreateIssue(Issue issue)
        {
            if (_preserveIssueId)
            {
                DeleteExistingIssue(issue.Id);
            }

            string requestContent = ConstructIssueRequest(issue);
            RestRequest request = new RestRequest($"issues?{requestContent}");
            request.Method = Method.POST;
            request.AddHeader("PRIVATE-TOKEN", _accessToken);

            string content;
            if (ClientPost(request, out content))
            {
                dynamic responseJson = JObject.Parse(content);
                int newId = Convert.ToInt32(responseJson.iid);

                foreach (Note note in issue.Notes)
                {
                    AddNote(newId, note);
                }

                if (issue.Status == "resolved" || issue.Status == "closed")
                {
                    CloseIssue(newId, issue.UpdatedAt);
                }

                if (_preserveIssueId)
                {
                    foreach (KeyValuePair<int, string> kv in issue.Relations)
                    {
                        if (kv.Value == "duplicate-of")
                        {
                            _relations.Add(new RelationData(issue.Id, kv.Key, true, issue.UpdatedAt));
                        }
                        else if (kv.Value == "related-to")
                        {
                            _relations.Add(new RelationData(issue.Id, kv.Key, false, issue.UpdatedAt));
                        }
                        else if (kv.Value == "child-of")
                        {
                            _relations.Add(new RelationData(issue.Id, kv.Key, false, issue.UpdatedAt));
                        }
                    }
                }

                return newId;
            }

            return -1;
        }

        void CloseIssue(int id, DateTime updated)
        {
            RestRequest request =
                new RestRequest($"issues/{id}/notes?body={EncodeString("/close")}&created_at={EncodeString(updated.ToString())}");
            request.Method = Method.POST;
            request.AddHeader("PRIVATE-TOKEN", _accessToken);

            string content;
            ClientPost(request, out content);
        }

        void AddRelation(int id, int otherId, bool isDuplicate, DateTime updated)
        {
            string content = string.Empty;
            if (isDuplicate)
            {
                content = EncodeString($"/duplicate #{otherId}");
            }
            else
            {
                content = EncodeString($"related to #{otherId}");
            }

            RestRequest request = new RestRequest($"issues/{id}/notes?body={content}&created_at={EncodeString(updated.ToString())}");
            request.Method = Method.POST;
            request.AddHeader("PRIVATE-TOKEN", _accessToken);
            client.Post(request);
        }

        string ConstructNoteContent(Note note)
        {
            string request = String.Empty;
            AddRequestElement(ref request, String.Format("body={0}", EncodeString(note.Content)));
            AddRequestElement(ref request, string.Format("created_at={0}", note.CreatedAt));
            return request;
        }

        void AddNote(int id, Note note)
        {
            string noteContent = ConstructNoteContent(note);
            AddNote(id, noteContent);
        }

        public void AddNote(int id, string text, DateTime created)
        {
            string content = $"body={EncodeString(text)}&created_at={created}";
            AddNote(id, content);
        }

        void AddNote(int id, string noteContent)
        {
            RestRequest request = new RestRequest($"issues/{id}/notes?{noteContent}");
            request.Method = Method.POST;
            request.AddHeader("PRIVATE-TOKEN", _accessToken);

            string content;
            ClientPost(request, out content);
        }

        void AddRequestElement(ref string request, string element)
        {
            if (request.Length > 0 && element.Length > 0)
            {
                request += "&";
            }

            request += element;
        }

        string EncodeString(string input)
        {
            input = input.Replace("\n", "\\\n");
            return Uri.EscapeDataString(input);
        }

        string ConstructIssueRequest(Issue issue)
        {
            string request = String.Empty;

            AddRequestElement(ref request, String.Format("title={0}", EncodeString(issue.Title)));
            AddRequestElement(ref request, String.Format("description={0}", EncodeString(issue.Description)));
            AddRequestElement(ref request, string.Format("created_at={0}", issue.CreatedAt));
            AddRequestElement(ref request, CreateLabels(issue));
            if (issue.TargetVersion != String.Empty || issue.FixedVersion != string.Empty)
            {
                AddRequestElement(ref request,
                    string.Format("milestone_id={0}",
                        GetMilestoneId(issue.Project, issue.TargetVersion, issue.FixedVersion)));
            }

            if (_preserveIssueId)
            {
                AddRequestElement(ref request, string.Format("iid={0}", issue.Id));
            }

            return request;
        }

        int GetMilestoneId(string project, string targetVersion, string fixedVersion)
        {
            string milestoneName;
            if (fixedVersion != string.Empty)
            {
                milestoneName = $"{project}_{fixedVersion}";
            }
            else
            {
                milestoneName = $"{project}_{targetVersion}";
            }

            if (!_milestoneDict.ContainsKey(milestoneName))
            {
                AddMilestone(milestoneName);
            }

            return _milestoneDict[milestoneName];
        }

        void InitMilestones()
        {
            _milestoneDict = new Dictionary<string, int>();
            RestRequest request = new RestRequest("milestones");
            request.Method = Method.GET;
            request.AddHeader("PRIVATE-TOKEN", _accessToken);

            string content;
            ClientGet(request, out content);

            JArray milestonesJson = JArray.Parse(content);
            foreach (dynamic milestone in milestonesJson)
            {
                _milestoneDict.Add(milestone.title.ToString(), Convert.ToInt32(milestone.id));
            }
        }

        void AddMilestone(string name)
        {
            RestRequest request = new RestRequest($"milestones?title={name}");
            request.Method = Method.POST;
            request.AddHeader("PRIVATE-TOKEN", _accessToken);

            string content;
            ClientPost(request, out content);

            dynamic milestoneJson = JObject.Parse(content);
            _milestoneDict.Add(milestoneJson.title.ToString(), Convert.ToInt32(milestoneJson.id));
        }

        void AddLabel(ref string labels, string label, string exclude1 = "", string exclude2 = "", string exclude3 = "")
        {
            if ((exclude1.Length > 0 && label == exclude1) ||
                (exclude2.Length > 0 && label == exclude2) ||
                (exclude3.Length > 0 && label == exclude3))
            {
                return;
            }

            if (label.Length > 0 && labels.Length > 0)
            {
                labels += ",";
            }

            labels += label;
        }

        string CreateLabels(Issue issue)
        {
            string labelContent = string.Empty;

            AddLabel(ref labelContent, issue.Project);
            AddLabel(ref labelContent, issue.Severity);
            AddLabel(ref labelContent, issue.Status, "resolved", "closed");
            AddLabel(ref labelContent, issue.Resolution, "open");
            AddLabel(ref labelContent, issue.Category, "other");

            if (labelContent.Length > 0)
            {
                return string.Format("labels={0}", EncodeString(labelContent));
            }
            else
            {
                return string.Empty;
            }
        }
    }
}