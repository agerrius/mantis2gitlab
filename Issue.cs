﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Mantis2Gitlab
{
    public class Issue
    {
        public bool IsEmpty { get; set; }
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string Category { get; set; }
        public string Severity { get; set; }
        public string Project { get; set; }
        public string TargetVersion { get; set; }
        public string FixedVersion { get; set; }
        public string Resolution { get; set; }
        public string Status { get; set; }
        public List<Note> Notes { get; set; }
        public List<Attachment> Attachments { get; set; }
        
        public Dictionary<int, string> Relations { get; set; }

        public Issue()
        {
            Notes = new List<Note>();
            Attachments = new List<Attachment>();
            Relations = new Dictionary<int, string>();
        }

        public void SaveAttachment(Attachment attachment, string filePath)
        {
            string folder = Path.GetDirectoryName(filePath);
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }

            BinaryWriter file = new BinaryWriter(new FileStream(filePath, FileMode.Create));
            byte[] data = attachment.Content;
            file.Write(data, 0, data.Length);
            file.Close();
        }
        
        public void Print()
        {
            Console.WriteLine("Issue Id:                {0}", Id);
            if (IsEmpty)
            {
                Console.WriteLine("Empty");
            }
            else
            {
                Console.WriteLine("Title:                   {0}", Title);
                Console.WriteLine("Description:             {0}", Description);
                Console.WriteLine("Created at:              {0}", CreatedAt);
                Console.WriteLine("Project:                 {0}", Project);
                Console.WriteLine("Category:                {0}", Category);
                Console.WriteLine("Severity:                {0}", Severity);
                Console.WriteLine("Status:                  {0}", Status);
                Console.WriteLine("Resolution:              {0}", Resolution);
                Console.WriteLine("Target version:          {0}", TargetVersion);
                Console.WriteLine("Fixed version:           {0}", FixedVersion);
                
                foreach(Note note in Notes)
                {
                    Console.WriteLine(">> ================================================");
                    note.Print();
                }

                foreach (Attachment attachment in Attachments)
                {
                    Console.WriteLine(">> ================================================");
                    attachment.Print();
                }

                Console.WriteLine(">> ================================================");
                foreach (KeyValuePair<int, string> kv in Relations)
                {
                    Console.WriteLine($">> {kv.Value} {kv.Key}");
                }
                
                Console.WriteLine(">> ================================================");
            }
        }
    }
}