﻿using System;

namespace Mantis2Gitlab
{
    public class Attachment
    {
        public string Name { get; set; }
        public byte[] Content { get; set; }
        public DateTime CreatedAt { get; set; }

        public void Print()
        {
            Console.WriteLine(">> Attachment: {0}", Name);
        }
    }
}