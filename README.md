# Mantis2Gitlab

When migrating some of my projects to Gitlab I need a way to get my issues from a Mantis bugtracker into Gitlab as well. As I could not find an easy way to do so, I made a little command line tool in C# that uses the Mantis API and Gitlab API to convert the issues.

* The tool will add milestones to Gitlab automatically based on the project and version information in Mantis.
* Mantis severity, categories and status are converted to labels in Gitlab
* Relations between issues are preserved as much as possible
* The tool has an option to download Mantis attachments (I could not find a way yet to automatically upload them to Gitlab)
* The tool can run in a mode where it preserves the issues IDs (this will overwrite existing issues in Gitlab with the same ID), but also in a mode where it will let Gitlab assign the ID.

Usage: \
Mantis2Gitlab <mantisUrl> <mantisToken> <gitlabUrl> <projectId> <gitlabToken> <fromID> <toID> <flags>

Possible flags: \
  -D <path> download Mants issue attachments to the given path \
  -P preserves the Mantis ID in Gitlab (existing issues are overwritten) \
  -V verbose mode, print issues to screen 
