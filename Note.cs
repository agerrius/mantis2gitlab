﻿using System;

namespace Mantis2Gitlab
{
    public class Note
    {
        public string Author { get; set; }
        public string Content { get; set; }
        public DateTime CreatedAt { get; set; }

        public void Print()
        {
            Console.WriteLine(">> NOTE by: {0}   at: {1}", Author, CreatedAt);
            Console.WriteLine(">> {0}", Content);
        }
    }
}