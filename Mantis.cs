﻿using System;
using RestSharp;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Mantis2Gitlab
{
    public class Mantis
    {
        private string _hostUrl;
        private string _accessToken;
        private RestClient client;
        private dynamic json;

        public Mantis(string hostURL, string accessToken)
        {
            _hostUrl = hostURL;
            _accessToken = accessToken;

            client = new RestClient($"{_hostUrl}/api/rest");
        }

        public Issue GetIssue(int issueId)
        {
            RestRequest request = new RestRequest($"issues/{issueId}");
            request.AddHeader("Authorization", _accessToken);

            var response = client.Get(request);
            if (!response.IsSuccessful)
            {
                return null;
            }
            else
            {
                json = JObject.Parse(response.Content);
                Issue issue = new Issue();
                issue.Id = issueId;
                FillIssue(issue);
                return issue;
            }
        }

        void FillIssue(Issue issue)
        {
            if (json.issues[0] != null)
            {
                dynamic issueJson = json.issues[0];

                issue.Title = GetTitle(issueJson);
                issue.Description = GetDescription(issueJson);
                issue.Project = GetProjectName(issueJson);
                issue.Status = GetStatusName(issueJson);
                issue.Resolution = GetResolutionName(issueJson);
                issue.Severity = GetSeverityName(issueJson);
                issue.Category = GetCategoryName(issueJson);
                issue.CreatedAt = GetCreatedAt(issueJson);
                issue.UpdatedAt = GetUpdatedAt(issueJson);
                issue.TargetVersion = GetTargetVersion(issueJson);
                issue.FixedVersion = GetFixedVersion(issueJson);
                issue.Notes = GetNotes(issueJson);
                issue.Attachments = GetAttachments(issueJson);
                issue.Relations = GetRelations(issueJson);
            }
            else
            {
                issue.IsEmpty = true;
            }
        }

        private string GetTitle(dynamic json)
        {
            if (json.summary != null)
            {
                return json.summary;
            }

            return string.Empty;
        }
        
        private string GetDescription(dynamic json)
        {
            if (json.description != null)
            {
                return json.description;
            }

            return string.Empty;
        }
        
        private string GetTargetVersion(dynamic json)
        {
            if (json.target_version != null)
            {
                return json.target_version.name;
            }

            return string.Empty;
        }
        
        private string GetFixedVersion(dynamic json)
        {
            if (json.fixed_version != null)
            {
                return json.fixed_version.name;
            }

            return string.Empty;
        }

        private DateTime GetCreatedAt(dynamic json)
        {
            if (json.created_at != null)
            {
                return DateTime.Parse(json.created_at.ToString());
            }

            return DateTime.Now;
        }

        private DateTime GetUpdatedAt(dynamic json)
        {
            if (json.updated_at != null)
            {
                return DateTime.Parse(json.updated_at.ToString());
            }

            return DateTime.Now;
        }

        private string GetSeverityName(dynamic json)
        {
            if (json.severity != null)
            {
                return json.severity.name;
            }

            return string.Empty;
        }

        private string GetCategoryName(dynamic json)
        {
            if (json.category != null)
            {
                return json.category.name;
            }

            return string.Empty;
        }

        private string GetResolutionName(dynamic json)
        {
            if (json.resolution != null)
            {
                return json.resolution.name;
            }

            return string.Empty;
        }

        private string GetStatusName(dynamic json)
        {
            if (json.status != null)
            {
                return json.status.name;
            }

            return string.Empty;
        }

        string GetProjectName(dynamic json)
        {
            if (json.project != null)
            {
                return json.project.name;
            }

            return string.Empty;
        }

        string GetFilename(dynamic json)
        {
            if (json.filename != null)
            {
                return json.filename;
            }

            return string.Empty;
        }

        string GetReporterName(dynamic json)
        {
            if (json.reporter != null)
            {
                return json.reporter.name;
            }

            return string.Empty;
        }

        string GetText(dynamic json)
        {
            if (json.text != null)
            {
                return json.text;
            }

            return String.Empty;
        }

        Dictionary<int, string> GetRelations(dynamic issue)
        {
            Dictionary<int, string> relations = new Dictionary<int, string>();

            if (issue.relationships != null)
            {
                JArray relationsJson = issue.relationships;
                foreach (dynamic relationJson in relationsJson)
                {
                    int issueId = Convert.ToInt32(relationJson.issue.id);
                    string relationType = relationJson.type.name;
                    relations.Add(issueId, relationType);
                }
            }
            
            return relations;
        }
        

        List<Attachment> GetAttachments(dynamic issue)
        {
            List<Attachment> attachments = new List<Attachment>();
            
            if (issue.attachments != null)
            {
                JArray attachmentsJson = issue.attachments;
                foreach (dynamic attachmentJson in attachmentsJson)
                {
                    Attachment attachment = new Attachment();
                    attachments.Add(attachment);

                    attachment.Name = GetFilename(attachmentJson);
                    attachment.Content = GetFileContent(Convert.ToInt32(issue.id),Convert.ToInt32(attachmentJson.id));
                    attachment.CreatedAt = GetCreatedAt(attachmentJson);
                }
            }

            return attachments;
        }

        byte[] GetFileContent(int issueId, int fileId)
        {
            RestRequest mantisFileRequest = new RestRequest($"issues/{issueId}/files/{fileId}");
            mantisFileRequest.AddHeader("Authorization", _accessToken);
            var fileResponse = client.Get(mantisFileRequest);

            dynamic fileJson = JObject.Parse(fileResponse.Content);
            return fileJson.files[0].content;
        }

        List<Note> GetNotes(dynamic issue)
        {
            List<Note> notes = new List<Note>();
            if (issue.notes != null)
            {
                JArray notesJson = issue.notes;
                foreach (dynamic noteJson in notesJson)
                {
                    Note note = new Note();
                    notes.Add(note);

                    note.Author = GetReporterName(noteJson);
                    note.Content = GetText(noteJson);
                    note.CreatedAt = GetCreatedAt(noteJson);
                }
            }

            return notes;
        }
    }
}